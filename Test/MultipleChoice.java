package Test;
/**
 * Multiple choice class to represent multiple choice question
 */
public class MultipleChoice extends Question {

    public MultipleChoice(String type, String question, String[]answerChoice, String rightAnswer){
        super(type, question, answerChoice, rightAnswer);
    }

    public MultipleChoice(String type, String question, String rightAnswer){
        super(type, question, rightAnswer);
    }

    /**
     *  Return a String representing a question for multiple choice
     */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(this.getType() + " question: " + this.getQuestion() + " Answer: ");

        // places array of possible answers below the above string
        for(String choice : this.getChoices()){
            sb.append("\n" + choice);
        }

        return sb.toString();
    }

    /**
     *  Return a String representing a question with an answer for multiple choice
     */
    @Override
    public String toStringWithAnswer(){
        StringBuilder sb = new StringBuilder();
        sb.append(this.getType() + " question: " + this.getQuestion() + " Answer: " + this.getAnswer());

        // places array of possible answers below the above string
        for(String choice : this.getChoices()){
            sb.append("\n" + choice);
        }

        return sb.toString();
    }
}
