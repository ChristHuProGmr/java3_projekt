package Test;
public class FillTheBlank extends Question{
    public FillTheBlank(String type, String question, String rightAnswer){
        super(type, question, rightAnswer);
    }

    /**
     * Most question are in the following format:
     * "True or false question: JVM is the abreviation for Java Virtual Machine Answer: "
     * The following method ommits " Answer: " for it is not needed in a Fill the Blank question.
     * @return
     */
    public String fbToStringNoAnswerField(){
        return this.getType() + " question: " + this.getQuestion();
    }
}
