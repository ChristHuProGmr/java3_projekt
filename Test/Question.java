package Test;


public abstract class Question {
    String questionString;
    String answer;
    String[] answerChoice;
    String questionType;


    protected Question(String type, String question, String answer) {
        this.questionString = question;
        this.answer = answer;
        this.questionType = type;
    }

    protected Question(String type, String question, String[] answerChoice, String answer) {
        this.questionString = question;
        this.answer = answer;
        this.answerChoice = answerChoice;
        this.questionType = type;
    }

    public String getQuestion() {
        return this.questionString;
    }

    // retrieve right answer to correction
    public String getAnswer() {
        return this.answer;
    }

    // retrieve question type 
    public String getType() {
        return this.questionType;
    }

    // returns array containing MultipleChoice's possible answers.
    public String[] getChoices() {
        return this.answerChoice;
    }

    @Override
    public String toString(){
        return getType() + " question: " + getQuestion() + " Answer: ";
    }

    public String toStringWithAnswer(){
        return getType() + " question: " + getQuestion() + " Answer: " + getAnswer();
    }
}
