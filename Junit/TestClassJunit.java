package Junit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import FileSaver.CorrectTest;
import Loader.DataExtractor;
import Loader.RawData;
import Test.Question;

/**
 * Test RawData class methods and Test correctTest class methods
 */
public class TestClassJunit {
    
    /**
     * Testing the sortData method and question class 
     */
    @Test
    public void rawDataTestAnswer(){
       
        List<String> question = new ArrayList<>();
        question.add("Multiple choice question: How large is FFX series maps?;choice:A: Too long. Less do nothing.;choice:B: Too long. Less do nothing.;choice:C: Too long. Less do nothing Answer: C");
        question.add("True or false question: The GCD in FFXIV is 2.5 seconds. Answer: true");
        question.add("Fill the blank question: 1 + _ = 1 Answer: 1 + 0 = 1");
        question.add("Short answer question: As of today, who is the head of the XBOX brand? Answer: Phil Spencer");

        DataExtractor data = new RawData(question);

        List<Question> mc = new ArrayList<>();
        mc = data.makeQuestions();  // testing data insertion into Question class
        
        // retriving answer from question class for each question type
        assertEquals("C", mc.get(0).getAnswer() );
        assertEquals("true", mc.get(1).getAnswer() );
        assertEquals("1 + 0 = 1", mc.get(2).getAnswer() );
        assertEquals("Phil Spencer", mc.get(3).getAnswer() );

    }
    @Test
    public void rawDataTestCorrectionAnswer(){
       
        List<String> question = new ArrayList<>();
        question.add("Multiple choice question: How large is FFX series maps?;choice:A: Too long. Less do nothing.;choice:B: Too long. Less do nothing.;choice:C: Too long. Less do nothing Answer: C");
        question.add("True or false question: The GCD in FFXIV is 2.5 seconds. Answer: true");
        question.add("Fill the blank question: 1 + _ = 1 Answer: 1 + 0 = 1");
        question.add("Short answer question: As of today, who is the head of the XBOX brand? Answer: Phil Spencer");

        DataExtractor data = new RawData(question);

        List<Question> mc = new ArrayList<>();
        mc = data.sortCorrectionData();  // testing sortCorrectionData by data insertion into Question class
        
        // retriving answer from question class for each question type
        assertEquals("C", mc.get(0).getAnswer() );
        assertEquals("true", mc.get(1).getAnswer() );
        assertEquals("1 + 0 = 1", mc.get(2).getAnswer() );
        assertEquals("Phil Spencer", mc.get(3).getAnswer() );

    }
    @Test
    public void rawDataTestType(){
        List<String> question = new ArrayList<>();
        question.add("Multiple choice question: How large is FFX series maps?;choice:A: Too long. Less do nothing.;choice:B: Too long. Less do nothing.;choice:C: Too long. Less do nothing Answer: C");
        question.add("True or false question: The GCD in FFXIV is 2.5 seconds. Answer: true");
        question.add("Fill the blank question: 1 + _ = 1 Answer: 1 + 0 = 1");
        question.add("Short answer question: As of today, who is the head of the XBOX brand? Answer: Phil Spencer");

        DataExtractor data = new RawData(question);

        List<Question> mc = new ArrayList<>();
        mc = data.makeQuestions();  // testing data insertion into Question class

        assertEquals("Multiple choice", mc.get(0).getType() );
        assertEquals("True or false", mc.get(1).getType() );
        assertEquals("Fill the blank", mc.get(2).getType() );
        assertEquals("Short answer", mc.get(3).getType() );

    }

    @Test
    public void rawDataTestCorrectionType(){
        List<String> question = new ArrayList<>();
        question.add("Multiple choice question: How large is FFX series maps?;choice:A: Too long. Less do nothing.;choice:B: Too long. Less do nothing.;choice:C: Too long. Less do nothing Answer: C");
        question.add("True or false question: The GCD in FFXIV is 2.5 seconds. Answer: true");
        question.add("Fill the blank question: 1 + _ = 1 Answer: 1 + 0 = 1");
        question.add("Short answer question: As of today, who is the head of the XBOX brand? Answer: Phil Spencer");

        DataExtractor data = new RawData(question);

        List<Question> mc = new ArrayList<>();
        mc = data.sortCorrectionData();  // testing sortCorrectionData by data insertion into Question class

        // retriving answer from question class for each question type
        assertEquals("Multiple choice", mc.get(0).getType() );
        assertEquals("True or false", mc.get(1).getType() );
        assertEquals("Fill the blank", mc.get(2).getType() );
        assertEquals("Short answer", mc.get(3).getType() );

    }

    @Test
    public void rawDataTestQuestion(){
        List<String> question = new ArrayList<>();
        question.add("Multiple choice question: How large is FFX series maps?;choice:A: Too long. Less do nothing.;choice:B: Too long. Less do nothing.;choice:C: Too long. Less do nothing Answer: C");
        question.add("True or false question: The GCD in FFXIV is 2.5 seconds. Answer: true");
        question.add("Fill the blank question: 1 + _ = 1 Answer: 1 + 0 = 1");
        question.add("Short answer question: As of today, who is the head of the XBOX brand? Answer: Phil Spencer");

        DataExtractor data = new RawData(question);

        List<Question> mc = new ArrayList<>(); 
        mc = data.makeQuestions(); // testing data insertion into Question class
      
        // retriving answer from question class for each question type
        assertEquals("How large is FFX series maps?", mc.get(0).getQuestion());
        assertEquals("The GCD in FFXIV is 2.5 seconds.", mc.get(1).getQuestion() );
        assertEquals("1 + _ = 1", mc.get(2).getQuestion() );
        assertEquals("As of today, who is the head of the XBOX brand?", mc.get(3).getQuestion() );
    }
    
    /**
     * Testing checkForRightAnswer method for correct comparison
     */
    @Test
    public void correctTestTest(){
        CorrectTest test = new CorrectTest();
        String correction = test.checkForRightAnswer("True or false", "T", "true");
        assertEquals("Good", correction);
        correction = test.checkForRightAnswer("True or false", "F", "true");
        assertEquals("Wrong", correction);
        
        correction = test.checkForRightAnswer("Multiple choice", "A", "B");
        assertEquals("Wrong", correction);
        
        correction = test.checkForRightAnswer("Multiple choice", "A", "A");
        assertEquals("Good", correction);
       
        correction = test.checkForRightAnswer("Fill the blank", ";", "T");
        assertEquals("Wrong", correction);
        
        correction = test.checkForRightAnswer("Fill the blank", ";", ";");
        assertEquals("Good", correction);
        
        correction = test.checkForRightAnswer("Short answer", "Phil Spencer", "Josh Peter");
        assertEquals("Wrong", correction);

        correction = test.checkForRightAnswer("Short answer", "Phil Spencer", "Phil Spencer");
        assertEquals("Good", correction);
    }
}
