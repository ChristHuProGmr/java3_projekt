import ActionHandlers.CorrectFileHandler;
import ActionHandlers.GenerateHandler;
import ActionHandlers.LabelFields;
import ActionHandlers.SavedFileHandler;
import javafx.application.*;
import javafx.scene.paint.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class TestGenApp extends Application {

    @Override
    public void start(Stage stage) {
        // container and layout
        Group root = new Group();

        // Grouping of labels for loading files
        LabelFields labels = new LabelFields("Load Question File:");
        Label count = new Label("Total File loaded: 0");

        VBox box1 = new VBox();
        box1.getChildren().addAll(labels, count);

        // Grouping of labels for loading files
        LabelFields labelsForTeacherCopy = new LabelFields("Save Correction File:");
      
        // Grouping of labels for saving files
        LabelFields labelsForSaving = new LabelFields("Save Test File:");
        Button saveBtn = new Button("Save File");
        Button generator = new Button("Randomize Question");
        Label text = new Label("false");

        // Generator handler
        GenerateHandler generate = new GenerateHandler(text);
        generator.setOnAction(generate);

        // Save file hander
        SavedFileHandler save = new SavedFileHandler(labels, labelsForTeacherCopy, labelsForSaving, count, text);
        saveBtn.setOnAction(save);

        // layout for all save file GUI elements
        VBox box2 = new VBox();
        box2.getChildren().addAll(labelsForSaving, saveBtn, generator, text);

        HBox hbFiles = new HBox();
        hbFiles.getChildren().addAll(box1, labelsForTeacherCopy, box2);

        // add spacing betwween boxes
        hbFiles.setSpacing(30);

        // Grouping of labels for loading teacher files
        LabelFields labelsTeacher = new LabelFields("Load Correction File:");

        // Grouping of labels for loading student files
        LabelFields labelsStudent = new LabelFields("Load Test File:");

        // Grouping of labels for saving corrected files
        LabelFields labelsForCorrection = new LabelFields("Save Corrected File:");
        Button correctBtn = new Button("Correct and save a File");

        // Grouping of labels for corrected file
        VBox box3 = new VBox();
        box3.getChildren().addAll(labelsForCorrection, correctBtn);

        // layout for all corrected file GUI elements
        HBox hbCorrectFiles = new HBox();
        hbCorrectFiles.getChildren().addAll(labelsTeacher, labelsStudent, box3);

        // add spacing betwween boxes
        hbCorrectFiles.setSpacing(30);

        // Corrected file handler
        CorrectFileHandler correctionFile = new CorrectFileHandler(labelsTeacher, labelsStudent, labelsForCorrection);
        correctBtn.setOnAction(correctionFile);

        HBox hb = new HBox();
        hb.getChildren().addAll(hbFiles, hbCorrectFiles);

        // add spacing betwween boxes
        hb.setSpacing(30);
        
        root.getChildren().add(hb);

        // scene is associated with container, dimensions
        Scene scene = new Scene(root, 1200, 300);
        scene.setFill(Color.WHITE);

        // associate scene to stage and show
        stage.setTitle("TestGenApp - PLEASE READ THE README");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }

}
