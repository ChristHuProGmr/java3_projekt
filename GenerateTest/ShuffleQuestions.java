package GenerateTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import Test.Question;

/**
 * Allow user to suffle questions.
 */
public class ShuffleQuestions {
    public List<Question> randomizeGen(List<Question> questionList) {
        // Hold a list of index to use for checking for duplication
        List<Integer> indexList = new ArrayList<>();
        Random rand = new Random();

        int length = questionList.size();
        // A list to hold a new shuffle question list 
        List<Question> newList = new ArrayList<>();

        boolean repeated = false;
        // Count current length of new shuffle question list
        int count = 0;

        // loop until newList length is equal to length 
        while (count != length) {

            int index = 0;
            // get a random index number
            index = rand.nextInt(length);

            // Add first index to newList
            if (indexList.size() == 0) {
                addToList(indexList, index, newList, questionList);
                count++;

            } else {
                // check if index is repeated
                repeated = checkForDuplicateIndex(indexList, index);
                // if index is not repeated then add to newList
                if (repeated == false) {
                    addToList(indexList, index, newList, questionList);
                    count++;
                }
                // reset repeated variable
                repeated = false;
            }
        }

        return newList;
    }
    /**
     * Check for duplicate index number
     * @param indexList list holding all unique index number that are currently added to newList
     * @param index current index number to be check if it is a duplicate 
     * @return boolean to indicate if index is duplicate or unique
     */
    public boolean checkForDuplicateIndex(List<Integer> indexList, int index) {
        for (int j : indexList) {
            if (index == j) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method to add a question to newList
     * @param indexList update index list
     * @param index current unique index number
     * @param newList new list that holds shuffle questions
     * @param questionList list that holds questions and it is not shuffle
     */
    public void addToList(List<Integer> indexList, int index, List<Question> newList, List<Question> questionList) {
        indexList.add(index);
        newList.add(questionList.get(index));

    }
}
