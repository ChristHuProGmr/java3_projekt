package Loader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import Test.Question;

public abstract class DataExtractor{

    /**
     * Method extracts lines from the List<Path> parameter into a List<String>
     * which is later returned.
     */
    public List<String> loadLinesFromFile(List<Path> paths) throws IOException{
        List<String> questions = new ArrayList<>();
        for(Path p : paths){
            questions.addAll(Files.readAllLines(p));
        }
        return questions;
    }
    
    /**
     * Method takes a String[] containing file addresses then uses the
     * data to populate and return a List<Path>
     */
    public List<Path> loadFiles(String[] filePath){
        List<Path> allPaths = new ArrayList<>();

        for(String file : filePath){
            Path p = Paths.get(file);
            allPaths.add(p);
        }

          
        return allPaths;
    }

    public abstract List<Question> makeQuestions();
    public abstract List<Question> sortCorrectionData();
}
