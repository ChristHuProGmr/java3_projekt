package Loader;
import java.util.ArrayList;
import java.util.List;

import Test.FillTheBlank;
import Test.MultipleChoice;
import Test.Question;
import Test.ShortAnswer;
import Test.TrueOrFalse;

public class RawData extends DataExtractor {
    List<String> data;

    public RawData() {
        this.data = null;
    }

    public RawData(List<String> data) {
        this.data = data;
    }

    /**
     * Method to sort question to a specific type and create its type of question
     */
    public List<Question> sortCorrectionData() {
        final int type = 0;
        List<Question> questionList = new ArrayList<>();
        String[] items;

       
        for (String question : this.data) {
            boolean isRowAQuestion = question.contains("question: ");
            if (isRowAQuestion) {
                items = question.split("( question: | Answer: )");
                switch (items[type]) {
                    case "True or false":
                        questionList.add(mkTrueOrFalseQuestion(items));
                        break;
                    case "Multiple choice":
                        questionList.add(mkMultipleChoiceQuestionToCorrect(items));
                        break;
                    case "Short answer":
                        questionList.add(mkShortAnswerQuestion(items));
                        break;
                    case "Fill the blank":
                        questionList.add(mkFillTheBlankQuestion(items));
                        break;
                    default:
                        throw new IllegalArgumentException("Invalid question type!");
                }
            }
        }

        return questionList;
    }

    /**
     * Method to create a MultipleChoice object and return it
     * @param items extracted question
     * @return MultipleChoice object with a contructor without possible answer
     */
    public MultipleChoice mkMultipleChoiceQuestionToCorrect(String[] items) {
        final int type = 0;
        final int description = 1;
        final int answer = items.length - 1;

        return new MultipleChoice(items[type], items[description], items[answer]);
    }

    /**
     * Method creates a series of Questions of different types from RawData's data
     * then returns it as List<Question>.
     * final int type represents the question type in the items array.
     * The indexes 0 and 1 contain the question type and description,
     * in addition, the final index contains the answer.
     */
    public List<Question> makeQuestions() {
        final int type = 0;
        List<Question> questionList = new ArrayList<>();
        String[] items;

        for (String question : this.data) {
            items = question.split("( question: | Answer: |;choice:)");
            switch (items[type]) {
            case "True or false":
                questionList.add(mkTrueOrFalseQuestion(items));
                break;
            case "Multiple choice":
                questionList.add(mkMultipleChoiceQuestion(items));
                break;
            case "Short answer":
                questionList.add(mkShortAnswerQuestion(items));
                break;
            case "Fill the blank":
                questionList.add(mkFillTheBlankQuestion(items));
                break;
            default:
                throw new IllegalArgumentException("Invalid question type!");
            }
        }

        return questionList;
    }

    /**
     * Make and return ShortAnswer Question
     * @param items
     * @return
     */
    private ShortAnswer mkShortAnswerQuestion(String[] items){
        final int type = 0;
        final int description = 1;
        final int answer = items.length - 1;
        return new ShortAnswer(items[type], items[description], items[answer]);
    }

    /**
     * Make and return TrueOrFalse Question
     * @param items
     * @return
     */
    private TrueOrFalse mkTrueOrFalseQuestion(String[] items) {
        final int type = 0;
        final int description = 1;
        final int answer = items.length - 1;
        return new TrueOrFalse(items[type], items[description], items[answer]);
    }

    /**
     * Make and return MultipleChoice Question
     * @param items
     * @return
     */
    private MultipleChoice mkMultipleChoiceQuestion(String[] items) {
        final int type = 0;
        final int description = 1;
        final int answer = items.length - 1;
        String[] choices = copyChoicesIntoArray(items);

        return new MultipleChoice(items[type], items[description], choices, items[answer]);
    }

    /**
     * Make and return FillTheBlank Question
     * @param items
     * @return
     */
    private FillTheBlank mkFillTheBlankQuestion(String[] items){
        final int type = 0;
        final int description = 1;
        final int answer = items.length - 1;
        return new FillTheBlank(items[type], items[description], items[answer]);
    }

    /**
     * Method returns an array that holds the possible answers in a
     * multiple choice question.
     * The indexes 0 and 1 contain the question type and description,
     * in addition, the final index contains the answer.
     * Thus, the method aims to extract Strings between 1 and items.length - 1
     * @param items
     * @return
     */
    private String[] copyChoicesIntoArray(String[] items) {
        final int startIndex = 2;
        String[] choices = new String[items.length - 3];
        int insIndex = 0;

        for (int i = startIndex; i < items.length - 1; i++) {
            choices[insIndex] = items[i];
            insIndex++;
        }

        return choices;
    }

}
