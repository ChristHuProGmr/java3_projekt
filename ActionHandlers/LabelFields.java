package ActionHandlers;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 * Holds labels representing basic GUI layout template
 */
public class LabelFields extends VBox {
    Label title;
    Label inputLabel;
    TextField path;
    Label outputLabel;
    public LabelFields(String title){
        this.title = new Label(title);
        this.inputLabel = new Label("Enter file path into text fields");
        this.path = new TextField();
        this.outputLabel = new Label("No file currently loaded");
        

        this.getChildren().addAll(this.title, this.inputLabel, this.path, this.outputLabel);
    }
}
