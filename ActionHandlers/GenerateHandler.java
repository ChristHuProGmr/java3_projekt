package ActionHandlers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;

/**
 * Handler to enable or disable shuffle question option
 */
public class GenerateHandler implements EventHandler<ActionEvent> {
    
    Label generate;

    public GenerateHandler(Label text) {
        this.generate = text;
    }
    @Override
    public void handle(ActionEvent e) {
        if(this.generate.getText().equals("true")){
            this.generate.setText("false");
        }else{
            this.generate.setText("true");
        }

    }
    
}
