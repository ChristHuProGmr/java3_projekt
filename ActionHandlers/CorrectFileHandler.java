package ActionHandlers;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import FileSaver.DataSaver;
import Loader.DataExtractor;
import Loader.RawData;
import Test.Question;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * Upload file and correct the test and save it
 */
public class CorrectFileHandler implements EventHandler<ActionEvent> {
    DataExtractor raw = new RawData();
    List<Question> test;
    List<Path> pArr;
    Label[] label;
    TextField[] path;

    public CorrectFileHandler(LabelFields lTeacher, LabelFields lStudent, LabelFields lCorrect) {
        this.path = new TextField[] { lTeacher.path, lStudent.path, lCorrect.path };
        this.label = new Label[] { lTeacher.outputLabel, lStudent.outputLabel, lCorrect.outputLabel };
    }

    @Override
    public void handle(ActionEvent e) {
        int teacher = 0;
        int student = 1;
        int correction = 2;
        loadFile(teacher);
        this.test = raw.sortCorrectionData(); // process data to create a question object 
        loadFile(student);
        saveCorrectFile(correction);
    }

    /**
     * Upload file to prepare to save
     * @param index index to retrieve correct GUI layout
     */
    private void loadFile(int index) {
       
        // load documents
        String[] attributes = this.path[index].getText().split(" ");
        pArr = this.raw.loadFiles(attributes);

        try {
            extractData();
            this.label[index].setText("File loaded successfully");

        } catch (IOException err) {
            this.label[index].setText("File loading fail");
        }

    }

    /**
     * Extract data from file(s) and store them in raw to process
     * @throws IOException file uploading error
     */
    private void extractData() throws IOException {

        // extract data from file(s)
        List<String> lines = this.raw.loadLinesFromFile(pArr);
        this.raw = new RawData(lines);

    }

    /**
     * Method to save corrected question list to a file 
     * @param index index to retrieve correct GUI layout
     */
    private void saveCorrectFile(int index) {
        List<Question> studentTest = raw.sortCorrectionData();

        // prepare for output
        DataSaver saver = new DataSaver();
        try {
            saver.saveCorrectedFile(this.test, studentTest, this.path[index].getText());
            this.label[index].setText("File saved successcully");
        } catch (IllegalArgumentException err) {
            this.label[index].setText("Cannot save to File");
        }
    }
}
