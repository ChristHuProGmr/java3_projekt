package ActionHandlers;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import FileSaver.DataSaver;
import GenerateTest.ShuffleQuestions;
import Loader.DataExtractor;
import Loader.RawData;
import Test.Question;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * Upload a file and Save one for teacher for reference and one for student to answer  
 */
public class SavedFileHandler implements EventHandler<ActionEvent> {
    DataExtractor raw = new RawData();
    List<Question> test;
    Label[] label;
    TextField[] path;
    List<Path> pArr;
    Label count;
    Label generate;

    public SavedFileHandler(LabelFields labels, LabelFields lTeacher, LabelFields lSaver, Label count, Label text) {
        this.path = new TextField[] { labels.path, lTeacher.path, lSaver.path };
        this.label = new Label[] { labels.outputLabel, lTeacher.outputLabel, lSaver.outputLabel };
        this.count = count;
        this.generate = text;
    }

    @Override
    public void handle(ActionEvent e) {
        int questionFile = 0;
        try{
            loadFile(questionFile);
            saveFile();
        } catch (IllegalArgumentException err2) {
            // check for duplicate file uploaded
            for(Label text : this.label){
                text.setText("Duplicate file name");
            }
        }

    }
    
    /**
     * Upload a file to be saved 
     * @param indexindex to retrieve correct GUI layout
     */
    private void loadFile(int index) {
        String[] attributes = this.path[index].getText().split(" ");

        // Prevent duplicate file name that has been uploaded
        for (int i = 0; i < attributes.length; i++) {
            for (int j = i + 1; j < attributes.length; j++)
                if (attributes[i].equals(attributes[j])) {
                        throw new IllegalArgumentException("Duplicate file name");
                    
                }
        }
        // load documents
        pArr = this.raw.loadFiles(attributes);
        // extract data from file(s)
        try {
            extractData();
            this.label[index].setText("File loaded successfully");

        } catch (IOException err) {
            this.label[index].setText("File loading fail");
        }
        this.count.setText("Total File loaded: " + attributes.length);

    }

    /**
     * Extract data from file(s) and store them in raw to process
     * @throws IOException file uploading error
     */
    private void extractData() throws IOException {

        // extract data from file(s)
        List<String> lines = this.raw.loadLinesFromFile(pArr);
        this.raw = new RawData(lines);

    }

    /**
     * Method to save question list to a file 
     */
    private void saveFile() {

        this.test = this.raw.makeQuestions();

        if(this.generate.getText().equals("true")){
            ShuffleQuestions gen = new ShuffleQuestions();

            this.test = gen.randomizeGen(this.test);
        }
        // prepare for output
        DataSaver saver = new DataSaver();
        int teacherFile = 1;
        int officialFile = 2;
        try {

            String[] listPath = { this.path[officialFile].getText(), this.path[teacherFile].getText() };
            saver.saveTestAndCorrectFiles(this.test, listPath);
            this.label[officialFile].setText("File saved successfully");
            this.label[teacherFile].setText("File saved successfully");

        } catch (IllegalArgumentException err) {
            this.label[officialFile].setText("Cannot save to File");
            this.label[teacherFile].setText("Cannot save to File");
        }
    }

}
