package FileSaver;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import Test.FillTheBlank;
import Test.Question;


public class DataSaver {

    /**
     * This method will write to disk a generated test and the associated correction file.
     * It is achieved by two passes of a for loop (one for the test the other for the answer sheet),
     * where it populates a List<String> then a function is called to permanently save them to disk.
     * @param test, List<Question>. Will populate the test and associated correct file with its data.
     * @param filePath, String[] of size two. Holds addresses of where to write related files.
     */
    public void saveTestAndCorrectFiles(List<Question> test, String[] filePath) {
        boolean processingCorrectionSheet = false;

        for(int i = 0; i < filePath.length; i++){
            List<String> outputSheet = populateSheet(test, processingCorrectionSheet);
            saveToFile(outputSheet, filePath[i]);
            processingCorrectionSheet = !processingCorrectionSheet;
        }
    }

    /**
     * Method fills and returns a List<String> using data from List<Question>
     * @param test
     * @param processingCorrectionSheet, determines whether to generate a test or correction sheet
     * @return
     */
    private List<String> populateSheet(List<Question> test, boolean processingCorrectionSheet){
        List<String> stringOutput = new ArrayList<>();
        for (Question question : test) {

            addString(question, stringOutput, processingCorrectionSheet);

        }
        return stringOutput;
    }

    /**
     * Method calls a Question object's toString then appends it to List<String>
     * @param question
     * @param stringOutput
     * @param processingCorrectionSheet
     */
    private void addString(Question question, List<String> stringOutput, boolean processingCorrectionSheet){
        if(processingCorrectionSheet){
            stringOutput.add(question.toStringWithAnswer());
        }
        // answer field is ommited for test sheet if question is fill the blank
        else{
            stringOutput.add(
                question instanceof FillTheBlank ? ((FillTheBlank)question).fbToStringNoAnswerField() : question.toString()
            );
        }
    }

    /**
     * Saving to a file
     * @param output file to save
     * @param filePath path to save file
     */
    private void saveToFile(List<String> output, String filePath){
        try {
            Files.write(Paths.get(filePath), output);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Saving a corrected version to a file
     * @param test list of question with correct answer
     * @param studentTest list of question with student answer
     * @param filePath path to save corrected answer
     */
    public void saveCorrectedFile(List<Question> test, List<Question> studentTest,  String filePath) {
            // new output
            List<String> output = new ArrayList<>();
       
       
            // correctTest class to check for right answer
            CorrectTest correct = new CorrectTest();
              
            // prepare for output for corrected file
            int length = studentTest.size();
            for (int i = 0; i < length; i++) {
                String questionType = studentTest.get(i).getType();
                String examQuestion = studentTest.get(i).getQuestion();
                String studentAnswer = studentTest.get(i).getAnswer();
                String teacherAnswer = test.get(i).getAnswer(); // refer to Test class
                String checkAnswer = correct.checkForRightAnswer(questionType, studentAnswer, teacherAnswer); // method to correct exam
                  
                output.add(questionType + " Question " + examQuestion + " Student Answer: " + studentAnswer + " Correct Answer: " + teacherAnswer + ", This is " + checkAnswer);  
      
                // refer to Test class
                if (test.get(i).getChoices() != null) {
                    for (String item : test.get(i).getChoices()){
                        output.add(item);
                    }
                }
            }
      
            // calculate grades
            int grades = correct.getGrades();
            output.add( "Grades: " + grades +"/" + length);
      
            // save to file
            saveToFile(output, filePath);
    }
}
