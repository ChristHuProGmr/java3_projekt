package FileSaver;
public class CorrectTest {
    private int grades;

    public CorrectTest(){
        this.grades = 0;
    }

    /**
     * This method will evaluate a question by comparing the student's answer to the correct answer.
     * If its correct, the grade will increment by one.
     * @param type, String representing the question type.
     * @param studentAnswer
     * @param correctAnswer
     * @return
     */
    public String checkForRightAnswer(String type, String studentAnswer, String correctAnswer){
        boolean correct;

        //Short and fill the blank questions have the same means of correction.
        switch(type){
            case "True or false":
                correct = correctTrueOrFalse(studentAnswer, correctAnswer);
                break;
            case "Multiple choice":
                correct = correctMultipleChoice(studentAnswer, correctAnswer);
                break;
            case "Short answer":
                correct = correctAnswerString(studentAnswer, correctAnswer);
                break;
            case "Fill the blank":
                correct = correctAnswerString(studentAnswer, correctAnswer);
                break;
            default:
                throw new IllegalArgumentException("question type is invalid");
        }

        if(correct){
            this.grades++;
            return "Good";
        }
        else{
            return "Wrong";
        }
    }

    /**
     * Method will evaluate a TrueOrFalse Question by comparing the first character indexes
     * of studentAnswer and correctAnswer.
     * @param studentAnswer
     * @param correctAnswer
     * @return
     */
    private boolean correctTrueOrFalse(String studentAnswer, String correctAnswer){
        if( !(correctAnswer.equalsIgnoreCase("true") || correctAnswer.equalsIgnoreCase("false")) ){
            throw new IllegalArgumentException("correct answer for true or false question is invalid");
        }
        char charCorrectAnswer = correctAnswer.toLowerCase().charAt(0);
        char charStudentAnswer = studentAnswer.toLowerCase().charAt(0);

        return (charCorrectAnswer == charStudentAnswer);
    }

    /**
     * Evaluates if a MultipleChoice Question is correct by comparing
     * studentAnswer to correctAnswer
     * @param studentAnswer
     * @param correctAnswer
     * @return
     */
    private boolean correctMultipleChoice(String studentAnswer, String correctAnswer){
        final int ONE_CHARACTER = 1;
        if(correctAnswer.length() != ONE_CHARACTER){
            throw new IllegalArgumentException("correct answer for multiple choice question is invalid");
        }

        return (correctAnswer.equalsIgnoreCase(studentAnswer));
    }

    private boolean correctAnswerString(String studentAnswer, String correctAnswer){
        return (studentAnswer.equalsIgnoreCase(correctAnswer));
    }

    public int getGrades(){
        return this.grades;
    }
}
